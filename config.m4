# Lightbringer's fvwm2 configuration, to be processed with m4 -P

m4_define(`LQ',`m4_changequote(<,>)`m4_dnl'
m4_changequote`'')
m4_define(`RQ',`m4_changequote(<,>)m4_dnl`
'm4_changequote`'')
#' <- fix syntax highlighting

#running on a VM?
#m4_define(VM, `yes')
m4_define(VM, `no')

#` <- fix syntax highlighting

#using guake for console (instead of xfce4-terminal)?
#m4_define(CONSOLE, `guake')
m4_define(CONSOLE, `xfce4-terminal')

#` <- fix syntax highlighting

# optional vertical widget bar width: 128
# rationale:
# standard dockapp width = 64
# typical gkrellm widths = 68, 72
# FvwmWinList is not very useful at these widths -> 128
# there's at least one gkrellm theme with a width of 64
# the blueheart one seems to be even narrower

############### settings

# this should precede any bindings, applications and modules
# remove '5' if you want ScrollLock to act as escape mod
IgnoreModifiers L25

#DesktopSize 3x3
DesktopSize 1x1
# desktops are numbered 0..fvwm_lastdesk
SetEnv fvwm_lastdesk 8
EwmhNumberOfDesktops 9
DesktopName 0 Main
DesktopName 1 Legion
DesktopName 2 Gaming
DesktopName 3 Aux1
DesktopName 4 Aux2
DesktopName 5 Aux3
DesktopName 6 Aux4
DesktopName 7 Hideout
DesktopName 8 Clean

# BackingStore breaks gvim hard... and SaveUnder doesn't seem to be doing anything
#Style * BackingStore
Style * SaveUnder

# prevent edge stuff from interfering with multihead, x2x, etc.
EdgeScroll 0 0
EdgeThickness 0

# may want to add MouseFocusClickRaises later
Style * SloppyFocus

# at least for vm
Style * ClickToFocusRaises

# make sure window titles are unique
Style * IndexedWindowName

Style * MinOverlapPlacement, ResizeOpaque
OpaqueMoveSize unlimited
Emulate Fvwm
HideGeometryWindow Never
# also investigate EdgeMoveResistance if this is not enough
# or increase from 4 to 8
Style * SnapAttraction 8 SameType ScreenAll

# seems to be necessary to snap windows to inter-display boundaries
# except it doesn't do shit
#Style * EdgeMoveResistance 8

Style * DecorateTransient, RaiseTransient, LowerTransient, StackTransientParent
Style * FPGrabFocusTransient, FPReleaseFocusTransient
Style * FPLenient
Style * WindowShadeSteps 0
Style * MwmDecor, MwmFunctions, OLDecor
Style * NoIcon
Style * EWMHPlacementUseWorkingArea, EWMHMaximizeUseWorkingArea
Style * EWMHPlacementUseDynamicWorkingArea, EWMHMaximizeUseDynamicWorkingArea

# make mouse pointer easier to hide
# reference: https://docs.huihoo.com/tkinter/tkinter-reference-a-gui-for-python/cursors.html
#            http://www.chiark.greenend.org.uk/doc/fvwm/html/commands/CursorStyle.html
CursorStyle TITLE sb_left_arrow
CursorStyle TOP top_tee

#Style panel StaysOnTop
Style panel Layer 5
#Style xfce4-panel StaysOnTop
Style xfce4-panel Layer 5
Style Xfce4-panel Layer 5
Style LbNarrowWinList Layer 5, Title, !Borders, !Handles
Style Guake Layer 6
Style guake Layer 6

# if window icons should be forced,
# see http://fvwmforums.org/beginnersguide/global.html

############### variables

SetEnv fvwm_img $[FVWM_USERDIR]/images
SetEnv fvwm_script $[FVWM_USERDIR]/scripts

SetEnv fvwm_defaultfont "xft:Droid Sans:Regular:size=10:antialias=True"
SetEnv fvwm_tinyfont "xft:Droid Sans:Regular:size=8:antialias=True"

SetEnv fvwm_titleheight 13
SetEnv fvwm_borderwidth 1

# window contents, all borders, all corners, all buttons, titlebar
# could have put it in an m4 macro, but i prefer envvar syntax for this
SetEnv fvwm_allwindowareas WFST138642

SetEnv fvwm_pseudomaxmargin 16

############### functions

#####
#
# DestroyFunc FuncName
# AddToFunc   FuncName
# + I (Action to happen immediately)
# + C (Action to happen on a mouse 'click)
# + D (Action to happen on a mouse 'double click')
# + H (Action to happen on a mouse 'hold')
# + M (Action to happen on a mouse 'motion')
#
###########

DestroyFunc LbQuakeConsole
AddToFunc   LbQuakeConsole
+ I Exec exec xfce4-terminal --drop-down
+ I Schedule 100 Next ("xfce4-terminal", Sticky) Focus NoWarp
# i would have preferred SkipList but it's not allowed here

DestroyFunc StartFunction
AddToFunc   StartFunction
+ I Module FvwmCommandS
+ I Exec exec xset m 1/1

DestroyFunc InitFunction
AddToFunc   InitFunction
#+ I Exec exec Esetroot -scale ~/m/pix/wallpaper.scale/4chan.org/1186552212658.jpg
+ I Exec exec $[fvwm_script]/fehbg.sh
#+ I Exec exec fbpanel
+ I Exec exec xfce4-panel
+ I Exec exec clipit
+ I Exec exec xfce4-power-manager
#+ I Exec exec xscreensaver
# for xfce4-terminal, not for guake
m4_ifelse(CONSOLE, `guake',
`+ I Exec exec guake',
`+ I LbQuakeConsole')

# no WarpToWindow
# D and H don't work here
DestroyFunc WindowListFunc
AddToFunc   WindowListFunc
+ I Iconify off
+ I FlipFocus
+ I Raise

DestroyFunc LbMoveOrRaiseOrLower
AddToFunc   LbMoveOrRaiseOrLower
+ H Raise
+ H Move
+ M Raise
+ M Move
+ C RaiseLower
+ I Nop

DestroyFunc LbRaiseLowerX
AddToFunc   LbRaiseLowerX
+ I Raise
+ M $0
+ D Lower

DestroyFunc LbUnDecorate
AddToFunc   LbUnDecorate
+ I WindowStyle !Title, !Borders, !Handles

DestroyFunc LbUnTitled
AddToFunc   LbUnTitled
+ I WindowStyle !Title, Borders, Handles

DestroyFunc LbDecorate
AddToFunc   LbDecorate
+ I WindowStyle Title, Borders, Handles

DestroyFunc LbUndecFullscreen
AddToFunc   LbUndecFullscreen
+ I ThisWindow LbUnDecorate
+ I Schedule 10 Maximize True emhiwa

DestroyFunc LbBorderFullscreen
AddToFunc   LbBorderFullscreen
+ I ThisWindow LbUnTitled
+ I Schedule 10 Maximize True emhiwa

DestroyFunc LbUndecFullscreenOntop
AddToFunc   LbUndecFullscreenOntop
+ I ThisWindow LbUndecFullscreen
+ I ThisWindow Layer 0 6

# undoes LbUndecFullscreenOntop fully
DestroyFunc LbDecorateUnmaximizePut
AddToFunc   LbDecorateUnmaximizePut
+ I ThisWindow Layer 0 4
+ I ThisWindow LbDecorate
+ I Schedule 10 Maximize False

DestroyFunc LbTitleOnly
AddToFunc   LbTitleOnly
+ I WindowStyle Title, !Borders, !Handles

# func'd so its settings don't end up in fbpanel config
DestroyFunc LbStartTransientPager
AddToFunc   LbStartTransientPager
#+ I Module FvwmPager -transient LbTransientPager *
+ I Module FvwmPager -transient LbTransientPager 0 $[fvwm_lastdesk]

DestroyFunc LbStartPermanentPager
AddToFunc   LbStartPermanentPager
#+ I Module FvwmPager *
+ I Module FvwmPager 0 $[fvwm_lastdesk]

# workaround for panels and their partial struts
# layer 5 is special.

DestroyFunc LbMaximizeHorizontally
AddToFunc   LbMaximizeHorizontally
+ I Maximize growonlayers 5 5 grow 0

DestroyFunc LbMaximizeVertically
AddToFunc   LbMaximizeVertically
+ I Maximize growonlayers 5 5 0 grow

DestroyFunc LbMaximize
AddToFunc   LbMaximize
+ I Maximize growonlayers 5 5 grow grow

# not to be used for alt+tab, but for all the others
DestroyFunc LbWindowList
AddToFunc   LbWindowList
+ I WindowList Root c c TitleForAllDesks SelectOnRelease

# normally moving a window from layer 6 to 4 would lower it under other layer 4 windows
# avoid that with this function
DestroyFunc LbToLayer4
AddToFunc   LbToLayer4
+ I Pick (CirculateHit) Layer 0 4
+ I Raise

# see arrange.py for details. usage: LbArrange hd hs vd vs
DestroyFunc LbArrange
AddToFunc   LbArrange
# unmaximize, otherwise wmctrl won't be allowed to resize the window
+ I Maximize False
#+ I Exec exec $[fvwm_script]/arrange.py $[fvwm_titleheight] $[fvwm_borderwidth] $[w.width] $[w.height] $[w.x] $[w.y] $0 $1 $2 $3
# run command synchronously, otherwise the pointer movement caused by menu navigation
# might change the focus
+ I PipeRead '$[fvwm_script]/arrange.py $[fvwm_titleheight] $[fvwm_borderwidth] $[w.width] $[w.height] $[w.x] $[w.y] grid $0 $1 $2 $3 1>&2'

# see arrange.py for details. usage: LbArrangePseudoMax
DestroyFunc LbArrangePseudoMax
AddToFunc   LbArrangePseudoMax
# unmaximize, otherwise wmctrl won't be allowed to resize the window
+ I Maximize False
# run command synchronously, otherwise the pointer movement caused by menu navigation
# might change the focus
+ I PipeRead '$[fvwm_script]/arrange.py $[fvwm_titleheight] $[fvwm_borderwidth] $[w.width] $[w.height] $[w.x] $[w.y] pseudomax $[fvwm_pseudomaxmargin] 1>&2'

# see arrange.py for details. usage: LbArrangePseudoMaxNoMargin
DestroyFunc LbArrangePseudoMaxNoMargin
AddToFunc   LbArrangePseudoMaxNoMargin
# unmaximize, otherwise wmctrl won't be allowed to resize the window
+ I Maximize False
# run command synchronously, otherwise the pointer movement caused by menu navigation
# might change the focus
+ I PipeRead '$[fvwm_script]/arrange.py $[fvwm_titleheight] $[fvwm_borderwidth] $[w.width] $[w.height] $[w.x] $[w.y] pseudomax 0 1>&2'


############### menus

# based on /usr/share/fvwm/ConfigFvwmSetup
# TODO: hotkeys for Raise, Lower

DestroyMenu LbMisc
AddtoMenu   LbMisc	"Miscellaneous" Title
+ "Actual Size FvwmWinList"	Module FvwmWinList
+ "Narrow FvwmWinList"		Module FvwmWinList LbNarrowWinList
+ "FvwmPager"			LbStartPermanentPager
+ "Transient FvwmPager"		LbStartTransientPager

DestroyMenu LbWindowDecorations
AddToMenu   LbWindowDecorations		"Decorate	Alt+F11" Title
+ "&Full decorations"		LbDecorate
+ "No title bar, &Border only"	LbUnTitled
+ "&No decorations"		LbUnDecorate
+ "&Title bar only"		LbTitleOnly
+ ""				Nop
+ "Undecorated Fullscreen"	LbUndecFullscreen
+ "Border only Fullscreen"	LbBorderFullscreen
+ ""				Nop
+ "Undecorated Fullscreen, OnTop	Alt+Shift+F11"	LbUndecFullscreenOntop
+ "Undo Above	Alt+Ctrl+Shift+F11"			LbDecorateUnmaximizePut

# move window to another desktop
DestroyMenu LbMoveToDesk
AddToMenu   LbMoveToDesk
+ "&0: $[desk.name0]"				MoveToDesk 0 0
+ "&1: $[desk.name1]"				MoveToDesk 0 1
+ "&2: $[desk.name2]"				MoveToDesk 0 2
+ "&3: $[desk.name3]"				MoveToDesk 0 3
+ "&4: $[desk.name4]"				MoveToDesk 0 4
+ "&5: $[desk.name5]"				MoveToDesk 0 5
+ "&6: $[desk.name6]"				MoveToDesk 0 6
+ "&7: $[desk.name7]"				MoveToDesk 0 7
+ "&8: $[desk.name8]"				MoveToDesk 0 8

# move and resize windows to predefined locations
DestroyMenu LbArrangeWindow2x2
AddToMenu   LbArrangeWindow2x2	"Arrange 2x2"	Title
+ "Upper left"					LbArrange 2 1 2 1
+ "Upper right"					LbArrange 2 2 2 1
+ "Lower left"					LbArrange 2 1 2 2
+ "Lower right"					LbArrange 2 2 2 2
DestroyMenu LbArrangeWindow3x2
AddToMenu   LbArrangeWindow3x2	"Arrange 3x2"	Title
+ "Top 1"					LbArrange 3 1 2 1
+ "Top 2"					LbArrange 3 2 2 1
+ "Top 3"					LbArrange 3 3 2 1
+ "Bottom 1"					LbArrange 3 1 2 2
+ "Bottom 2"					LbArrange 3 2 2 2
+ "Bottom 3"					LbArrange 3 3 2 2
DestroyMenu LbArrangeWindow4x2
AddToMenu   LbArrangeWindow4x2	"Arrange 4x2"	Title
+ "Top 1"					LbArrange 4 1 2 1
+ "Top 2"					LbArrange 4 2 2 1
+ "Top 3"					LbArrange 4 3 2 1
+ "Top 4"					LbArrange 4 4 2 1
+ "Bottom 1"					LbArrange 4 1 2 2
+ "Bottom 2"					LbArrange 4 2 2 2
+ "Bottom 3"					LbArrange 4 3 2 2
+ "Bottom 4"					LbArrange 4 4 2 2
DestroyMenu LbArrangeWindow4x3
AddToMenu   LbArrangeWindow4x3	"Arrange 4x3"	Title
+ "Top 1"					LbArrange 4 1 3 1
+ "Top 2"					LbArrange 4 2 3 1
+ "Top 3"					LbArrange 4 3 3 1
+ "Top 4"					LbArrange 4 4 3 1
+ "Middle 1"					LbArrange 4 1 3 2
+ "Middle 2"					LbArrange 4 2 3 2
+ "Middle 3"					LbArrange 4 3 3 2
+ "Middle 4"					LbArrange 4 4 3 2
+ "Bottom 1"					LbArrange 4 1 3 3
+ "Bottom 2"					LbArrange 4 2 3 3
+ "Bottom 3"					LbArrange 4 3 3 3
+ "Bottom 4"					LbArrange 4 4 3 3
DestroyMenu LbArrangeWindow3x1
AddToMenu   LbArrangeWindow3x1	"Arrange 3x1"	Title
+ "Left"					LbArrange 3 1 1 1
+ "Middle"					LbArrange 3 2 1 1
+ "Right"					LbArrange 3 3 1 1
DestroyMenu LbArrangeWindow
AddToMenu   LbArrangeWindow	"Arrange"	Title
+ "&Left half"					LbArrange 2 1 1 1
+ "&Right half"					LbArrange 2 2 1 1
+ "&Top half"					LbArrange 1 1 2 1
+ "&Bottom half"				LbArrange 1 1 2 2
+ ""						Nop
+ "&2x2"					Popup LbArrangeWindow2x2
+ "&3x2"					Popup LbArrangeWindow3x2
+ "&4x2"					Popup LbArrangeWindow4x2
+ "&4x3"					Popup LbArrangeWindow4x3
+ "3x&1"					Popup LbArrangeWindow3x1
+ ""						Nop
+ "&PseudoMaximize"				LbArrangePseudoMax
+ "&PseudoMaximize without margins"		LbArrangePseudoMaxNoMargin
+ ""						Nop
+ "Edit arrange.py"				Exec exec gvim $[fvwm_script]/arrange.py

DestroyMenu LbWindowOps
AddToMenu   LbWindowOps		"Window Operations	Alt+F3" Title
+ "$[gt.&Move]	Alt+F7"				Move
+ "$[gt.&Resize]	Alt+F8"			Resize
+ "$[gt.Raise]"              			Raise
+ "$[gt.&Lower]"              			Lower
+ "$[gt.(De)&Iconify]	Alt+F9"			Iconify
+ "$[gt.(Un)&Stick]"				Stick
+ "(Un)Shade"					WindowShade
#+ "$[gt.(Un)Ma&ximize]	Alt+F10"		Maximize
+ "$[gt.(Un)Ma&ximize]	Alt+F10"		LbMaximize
#+ "(Un)Maximize Horizontally	Alt+Ctrl+F10"	Maximize 100 0
+ "(Un)Maximize Horizontally	Alt+Ctrl+F10"	LbMaximizeHorizontally
#+ "(Un)Maximize Vertically	Alt+Shift+F10"	Maximize 0 100
+ "(Un)Maximize Vertically	Alt+Shift+F10"	LbMaximizeVertically
+ "(Un)Grow"					Maximize grow grow
+ "(Un)Grow Horizontally"			Maximize grow 0
+ "(Un)Grow Vertically"				Maximize 0 grow
+ "Decorate...	Alt+F11"			Popup LbWindowDecorations
+ "Move &to desktop..."				Popup LbMoveToDesk
+ "&Arrange..."					Popup LbArrangeWindow
+ "&PseudoMaximize	Alt+F12"		LbArrangePseudoMax
+ ""						Nop
+ "Stays on Top (layer 6)"			Pick (CirculateHit) Layer 0 6
+ "Panel (layer 5)"				Pick (CirculateHit) Layer 0 5
+ "Layer +1"					Pick (CirculateHit) Layer +1
#+ "Stays Put (layer 4)"				Pick (CirculateHit) Layer 0 4
+ "Stays Put (layer 4)"				LbToLayer4
+ "Layer -1"					Pick (CirculateHit) Layer -1
+ "Stays on Bottom (layer 2)"			Pick (CirculateHit) Layer 0 2
+ ""						Nop
+ "$[gt.Re&fresh Window]"			RefreshWindow
+ Scroll&Bar					Module FvwmScroll 2 2
#+ "&$[gt.Print]"				FuncFvwmPrint
#+ "$[gt.Print Re&verse]"			FuncFvwmPrintReverse
+ "Identify"					Module FvwmIdent
+ "Root Menu	Alt+F1"				Popup MenuFvwmRoot
+ "Window List	Alt+F2"				LbWindowList
+ ""						Nop
+ "$[gt.&Delete]"				Delete
+ "$[gt.&Close]	Alt+F4"				Close
+ "$[gt.Destroy]"				Destroy

DestroyMenu LbKeyboardLayout
AddToMenu   LbKeyboardLayout	"Keyboard Layout"	Title
+ "(&1) us altgr-intl compose:102"		Exec exec setxkbmap us altgr-intl compose:102
+ "(&2) us basic compose:102"			Exec exec setxkbmap us basic compose:102
+ "(&3) us (no compose)"			Exec exec setxkbmap us
+ "(&4) Russian phonetic (us rus compose:102)"	Exec exec setxkbmap us rus compose:102
+ "(&5) gr basic compose:102"			Exec exec setxkbmap gr basic compose:102
+ ""						Nop
+ "Preferred choice for &Xonotic (us basic compose:102)"	Exec exec setxkbmap us basic compose:102

DestroyMenu LbFocusPolicy
AddToMenu   LbFocusPolicy	"Focus Policy"	Title
+ "&SloppyFocus"		Style * SloppyFocus
+ "&ClickToFocus"		Style * ClickToFocus
+ "&MouseFocus"			Style * MouseFocus

# all this is in addition to the default built-in root menu
AddToMenu MenuFvwmRoot ""			Nop
AddToMenu MenuFvwmRoot "Launch Terminator"      Exec exec terminator
AddToMenu MenuFvwmRoot "Window Operations	Alt+F3" Popup LbWindowOps
AddToMenu MenuFvwmRoot "Window List	Alt+F2" LbWindowList
AddToMenu MenuFvwmRoot "Miscellaneous..."	Popup LbMisc
AddToMenu MenuFvwmRoot "(&9) Keyboard Layout..."	Popup LbKeyboardLayout
AddToMenu MenuFvwmRoot "Pop up Quake-like Console	Super+LQ()"	LbQuakeConsole
AddToMenu MenuFvwmRoot "Focus Policy..."		Popup LbFocusPolicy


############### bindings
# global
# ideas (not followed strictly) from http://www.icewm.org/manual/icewm-6.html
Key Tab A M WindowList Root c c SelectOnRelease Meta_L CurrentAtEnd CurrentDesk
Key Tab A MS WindowList Root c c SelectOnRelease Meta_L CurrentAtEnd TitleForAllDesks
Key Tab A 4 WindowList Root c c Alphabetic SortByResource CurrentDesk
Key F1 A M Menu MenuFvwmRoot
Key F2 A M LbWindowList
Key F3 A M Menu LbWindowOps
Key F4 $[fvwm_allwindowareas] M Close
Key F7 $[fvwm_allwindowareas] M Move
Key F8 $[fvwm_allwindowareas] M Resize
Key F9 $[fvwm_allwindowareas] M Iconify
#Key F10 $[fvwm_allwindowareas] M Maximize
Key F10 $[fvwm_allwindowareas] M LbMaximize
#Key F10 $[fvwm_allwindowareas] MC Maximize 100 0
Key F10 $[fvwm_allwindowareas] MC LbMaximizeHorizontally
#Key F10 $[fvwm_allwindowareas] MS Maximize 0 100
Key F10 $[fvwm_allwindowareas] MS LbMaximizeVertically
Key F11 $[fvwm_allwindowareas] M Popup LbWindowDecorations
Key F11 $[fvwm_allwindowareas] MS LbUndecFullscreenOntop
Key F11 $[fvwm_allwindowareas] MCS LbDecorateUnmaximizePut
Key F12 $[fvwm_allwindowareas] M LbArrangePseudomax
Key F1 A 4 GotoDesk 0 0
Key F2 A 4 GotoDesk 0 1
Key F3 A 4 GotoDesk 0 2
Key F4 A 4 GotoDesk 0 3
Key F5 A 4 GotoDesk 0 4
Key F6 A 4 GotoDesk 0 5
Key F7 A 4 GotoDesk 0 6
Key F8 A 4 GotoDesk 0 7
Key F9 A 4 GotoDesk 0 8
Key Prior A 4 GotoDesk -1 0 $[fvwm_lastdesk]
Key Next A 4 GotoDesk +1 0 $[fvwm_lastdesk]
# for now, any (A) modifier is allowed here
Key XF86Back A A GotoDesk -1 0 $[fvwm_lastdesk]
Key XF86Forward A A GotoDesk +1 0 $[fvwm_lastdesk]
# for xfce4-terminal, not for guake
m4_ifelse(CONSOLE, `xfce4-terminal', `Key grave A 4 LbQuakeConsole')
#' <- fix syntax highlighting
# for vm, vnc, etc
m4_ifelse(VM, `yes', `Key 1 A 4 LbQuakeConsole')
#' <- fix syntax highlighting
# they both fucking fail on tomeofpower, so what the fuck can i do
#Key L A 4 Exec exec xscreensaver-command -lock
#Key L A 4 Exec exec xlock -mode blank
Key L A 4 Exec exec i3lock -c 404040

# root window clicks
Mouse 1 R N Menu MenuFvwmRoot
Mouse 2 R N LbWindowList
Mouse 3 R N Menu LbWindowOps

# alt+window clicks/drags
Mouse 1 $[fvwm_allwindowareas] M LbMoveOrRaiseOrLower
Mouse 2 $[fvwm_allwindowareas] M Menu LbWindowOps
Mouse 3 $[fvwm_allwindowareas] M LbRaiseLowerX Resize

# window decorations
# buttons: [1:resize][3:menu]   [8:iconify][6:grow][4:maximize][2:close]
# resize can be restricted to horizontal/vertical only by moving
#   outward only in that direction
Mouse 3 FST N Menu LbWindowOps
Mouse 2 FST N Lower
Mouse 4 T138642 N WindowShade True
Mouse 5 T138642 N WindowShade False
Mouse 1 1 N LbRaiseLowerX Resize
Mouse 1 3 N Menu LbWindowOps
Mouse 2 3 N LbWindowList
Mouse 3 3 N Menu MenuFvwmRoot
Mouse 1 8 N Iconify
#Mouse 1 4 N Maximize
Mouse 1 4 N LbMaximize
#Mouse 2 4 N Maximize 0 100
Mouse 2 4 N LbMaximizeVertically
#Mouse 3 4 N Maximize 100 0
Mouse 3 4 N LbMaximizeHorizontally
#Mouse 1 4 S Maximize 0 100
Mouse 1 4 S LbMaximizeVertically
#Mouse 1 4 C Maximize 100 0
Mouse 1 4 C LbMaximizeHorizontally
Mouse 1 6 N Maximize grow grow
Mouse 2 6 N Maximize 0 grow
Mouse 3 6 N Maximize grow 0
Mouse 1 6 S Maximize 0 grow
Mouse 1 6 C Maximize grow 0
Mouse 1 2 N Close
Mouse 1 2 S Delete
Mouse 1 2 C Destroy


############### appearance
# plan:
# title font: Droid Sans 8 regular -> 11px high -> titlebar will be 13px high (+1 or 2 black border)
# icons for title bar buttons are 7x7px
# buttons themselves could be square or wider
# TODO: write a decor(?) for vertical title bars, try them out, add them to menu or something

# title-like
Colorset 1 fg White, bg rgb:00/00/ff

# readable in bright light -- menus
Colorset 2 fg Black, bg rgb:ee/ee/ee
# menu highlighted
Colorset 3 fg Black, bg rgb:ba/d8/ff

# borders
Colorset 4 fg Black, bg Black
# borders of focused window
#Colorset 5 fg rgb:00/00/a0, bg rgb:00/00/a0
Colorset 5 fg Black, bg Black

# buttons: [1:resize][3:menu]   [8:iconify][6:grow][4:maximize][2:close]
DestroyDecor LbDecor
AddToDecor   LbDecor
+ TitleStyle LeftJustified Height $[fvwm_titleheight]
+ ButtonStyle 1 \
	ActiveUp	(Pixmap $[fvwm_img]/button/resize.png -- Flat) \
	ActiveDown	(Pixmap $[fvwm_img]/button/resize.png -- Raised) \
	Inactive	(Simple -- Flat)
+ ButtonStyle 3 \
	ActiveUp	(Pixmap $[fvwm_img]/button/menu.png -- Flat) \
	ActiveDown	(Pixmap $[fvwm_img]/button/menu.png -- Raised) \
	Inactive	(Simple -- Flat)
+ ButtonStyle 8 \
	ActiveUp	(Pixmap $[fvwm_img]/button/iconify.png -- Flat) \
	ActiveDown	(Pixmap $[fvwm_img]/button/iconify.png -- Raised) \
	Inactive	(Simple -- Flat)
+ ButtonStyle 6 \
	ActiveUp	(Pixmap $[fvwm_img]/button/grow2.png -- Flat) \
	ActiveDown	(Pixmap $[fvwm_img]/button/grow2.png -- Raised) \
	Inactive	(Simple -- Flat)
+ ButtonStyle 4 \
	ActiveUp		(Pixmap $[fvwm_img]/button/maximize.png -- Flat) \
	ActiveDown		(Pixmap $[fvwm_img]/button/maximize.png -- Raised) \
	ToggledActiveUp		(Pixmap $[fvwm_img]/button/maximize-toggled.png -- Flat) \
	ToggledActiveDown	(Pixmap $[fvwm_img]/button/maximize-toggled.png -- Raised) \
	Inactive		(Simple -- Flat)
+ ButtonStyle 2 \
	ActiveUp	(Pixmap $[fvwm_img]/button/close.png -- Flat) \
	ActiveDown	(Pixmap $[fvwm_img]/button/close.png -- Raised) \
	Inactive	(Simple -- Flat)
+ ButtonStyle 1 - Clear
+ ButtonStyle 3 - Clear MwmDecorMenu
+ ButtonStyle 8 - Clear MwmDecorMin
+ ButtonStyle 4 - Clear MwmDecorMax
+ ButtonStyle 2 - Clear
#+ TitleStyle (VGradient 13 rgb:00/00/ff rgb:ff/00/ff) -- Flat
#+ TitleStyle (VGradient 13 rgb:00/00/aa rgb:aa/00/aa) -- Flat
#+ TitleStyle (VGradient 13 2 rgb:00/00/40 50 rgb:00/00/a0 50 rgb:a0/00/a0) -- Flat
#+ TitleStyle (VGradient 13 3 rgb:00/00/c8 1 rgb:50/00/c8 2 rgb:96/00/c8 1 rgb:c8/00/c8) -- Flat
#+ TitleStyle (VGradient 13 rgb:41/00/c8 rgb:ad/00/c8) -- Flat
+ TitleStyle (VGradient 13 rgb:18/00/c8 rgb:ad/00/c8) -- Flat
+ BorderStyle Simple -- NoInset Flat
+ ButtonStyle All -- UseTitleStyle

Style * UseDecor LbDecor
Style * Font "$[fvwm_tinyfont]"
Style * BorderWidth $[fvwm_borderwidth], HandleWidth $[fvwm_borderwidth]
Style * FvwmBorder, FirmBorder
Style * Colorset 1
Style * HilightColorset 1
Style * BorderColorset 4
Style * HilightBorderColorset 5

MenuStyle * Fvwm
MenuStyle * PopupImmediately, PopdownImmediately, !Animation
MenuStyle * !TitleWarp
# automatic hotkeys are broken, blindly makes ambiguous hotkeys
MenuStyle * !AutomaticHotkeys
MenuStyle * MenuColorset 2, ActiveColorset 3, HilightBack, TrianglesSolid
MenuStyle * TrianglesUseFore
MenuStyle * BorderWidth 1, TitleUnderlines2
MenuStyle * PopupOffset -5 100
MenuStyle * Font "$[fvwm_defaultfont]"


############### modules

DestroyModuleConfig FvwmIdent: *
*FvwmIdent: Colorset 2
*FvwmIdent: Font "$[fvwm_defaultfont]"

DestroyModuleConfig LbTransientPager: *
*LbTransientPager: Font "$[fvwm_defaultfont]"
#*LbTransientPager: Geometry 256x0
# the alleged feature that lets geometry keep correct aspect ratio is broken
*LbTransientPager: DeskTopScale 16
*LbTransientPager: Columns 3

Style LbTransientPager StaysOnTop

DestroyModuleConfig FvwmPager: *
*FvwmPager: Font "$[fvwm_defaultfont]"
#*FvwmPager: Geometry 128x0
# assuming 3 columns (v.desktops are 3 pages wide)
# for a display width of 1920, DeskTopScale 45 gives a 128 pixel wide window
# for a display width of 1024, DeskTopScale 24 does the same
# (but does this include borders? probably not)
# for different numbers of columns, these numbers are c*15 and c*8
*FvwmPager: DeskTopScale 24
*FvwmPager: Columns 3

DestroyModuleConfig LbNarrowWinList: *
*LbNarrowWinList: Font "$[fvwm_tinyfont]"
*LbNarrowWinList: MaxWidth 128
*LbNarrowWinList: ShowCurrentDesk
*LbNarrowWinList: Action Click3 Popup LbWindowOps
*LbNarrowWinList: ButtonFrameWidth 1

DestroyModuleConfig FvwmWinList: *
*FvwmWinList: Font "$[fvwm_defaultfont]"
# default was Lower, apparently
*FvwmWinList: Action Click3 Popup LbWindowOps
*FvwmWinList: ButtonFrameWidth 1

